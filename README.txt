# OG Privacy

This module allows you to easily define the rules by which organic group posts will be visible to users that are not members of 
the groups to which they are posted.

This module came about at my need to override the Spaces OG module's aggressive control over the privacy user interface. I 
wanted publicly accessible groups in Open Atrium that had lots of content available only to group members.

It provides a policy creation API that sits above both OG Access and Spaces OG to override the privacy setting of group posts 
with what OG Privacy thinks that setting should be.

Policy creation follows the pattern of hook_menu() [see below for example], and allows you to define access callbacks that will 
be evaluated at node_save to determine how the node's privacy status should be set (regardless of what Spaces OG has to say). 
Once that privacy setting has been put in place and the node is saved, the standard, OG Access-directed content permissions 
routine will be run over the node.

## Multiple Policies

OG Privacy's *hook_og_privacy_policy_info()* hook supports multiple policies from one or more nodes, allow really complex node 
access routines to be define based arbitrarily on the data held in the node object. This can include everything from the node 
content type to post date to taxonomy terms.

If multiple policies are in place, by default they are non-exclusive. This means they will be OR'd together, and any policy 
indicating that public access should be granted will win out. There is an optional 'exclusive' property a policy can declare to 
make itself an exclusive rule. Exclusive policies have ultimate veto power over public node access of a group post.

## Shipped Features

OG Privacy is currently packaged with a complete feature which adds a checkbox widget back to group posts in Open Atrium to 
allow them to be individually toggled to public or private. It's an opt-in feature by group, meaning you don't have to trouble 
members of all groups on the site with the extra power and UI this provides.

## Installation Notes

1. Turn on OG Privacy.
2. Turn on a policy-provider module (such as OG Privacy for Open Atrium).

## Features in this Project
In the features directory you will find OG Privacy for Open Atrium, a module that gives you a nice widget to control whether a 
post is public. Be sure to read it's readme for installation instructions.

## Maintainers

* Grayside <grayside@gmail.com> [creator]