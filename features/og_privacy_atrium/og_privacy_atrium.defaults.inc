<?php

/**
 * Helper to implementation of hook_content_default_fields().
 */
function _og_privacy_atrium_content_default_fields() {
  $fields = array();

  // Exported field: field_og_privacy_atrium_allow
  $fields[] = array(
    'field_name' => 'field_og_privacy_atrium_allow',
    'type_name' => 'group',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'default|Use Group Settings
private|Per Node Privacy: Default Private
public|Per Node Privacy: Default Public',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => 'default',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Per Node Privacy Settings',
      'weight' => '1',
      'description' => 'Use advanced privacy settings. Group settings are uniformly open to the Library or restricted to group members. The other options add a toggle on every group post form to set whether the post will be visible to non-group members.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Translatables
  array(
    t('Per Node Privacy Settings'),
  );

  return $fields;
}

/**
 * Helper to implementation of hook_ctools_plugin_api().
 */
function _og_privacy_atrium_ctools_plugin_api() {
  $args = func_get_args();
  $module = array_shift($args);
  $api = array_shift($args);
  if ($module == "mark" && $api == "mark") {
    return array("version" => 1);
  }
}

/**
 * Helper to implementation of hook_mark_default_marks().
 */
function _og_privacy_atrium_mark_default_marks() {
  $export = array();
  $mark = new stdClass;
  $mark->disabled = FALSE; /* Edit this to true to make a default mark disabled initially */
  $mark->api_version = 1;
  $mark->name = 'og_privacy_atrium_public_node';
  $mark->basetable = 'node';
  $mark->settings = array(
    'title' => 'og_privacy_atrium_public_node',
    'mark_text' => 'Make Post Public',
    'mark_confirm_text' => 'The post will be viewable by non-group members.',
    'unmark_text' => 'Make Post Private',
    'unmark_confirm_text' => 'The post will be viewable only by group members.',
    'global' => 1,
    'form_widget' => 1,
    'form_widget_advanced' => array(
      'show_message' => 1,
      'allowed_types' => array(
        'book' => 0,
        'casetracker_basic_case' => 0,
        'blog' => 0,
        'event' => 0,
        'group' => 0,
        'presentation' => 0,
        'profile' => 0,
        'casetracker_basic_project' => 0,
        'shoutbox' => 0,
        'project_ticket' => 0,
        'feed_ical_item' => 0,
        'feed_ical' => 0,
      ),
    ),
  );

  $export['og_privacy_atrium_public_node'] = $mark;
  return $export;
}
