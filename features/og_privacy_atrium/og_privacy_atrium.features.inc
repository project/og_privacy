<?php

/**
 * Implementation of hook_content_default_fields().
 */
function og_privacy_atrium_content_default_fields() {
  module_load_include('inc', 'og_privacy_atrium', 'og_privacy_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_og_privacy_atrium_content_default_fields', $args);
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function og_privacy_atrium_ctools_plugin_api() {
  module_load_include('inc', 'og_privacy_atrium', 'og_privacy_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_og_privacy_atrium_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_mark_default_marks().
 */
function og_privacy_atrium_mark_default_marks() {
  module_load_include('inc', 'og_privacy_atrium', 'og_privacy_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_og_privacy_atrium_mark_default_marks', $args);
}
