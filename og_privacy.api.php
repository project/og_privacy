<?php

/**
 * Implementation of hook_spaces_og_access_policy_info().
 *
 * @param $node
 *  The node object to be affected by the access policies.
 *
 * @return Array
 * Returns an associative array of arrays with the following elements:
 *  - 'access callback': (string) Callback function by which access to a given node will be determined.
 *    Access callback should return TRUE if access would be granted, and FALSE if it would be denied.
 *  - 'exclusive': (boolean) If TRUE, the access callback will be able to veto access regardless of other policies.
 *  - 'reason': (string) Explain why access denied for a given node.
 */
function hook_og_privacy_policy_info($node) {
  $policies = array();

  $policies['field_grant_access'] = array(
    'access callback' => 'hook_policy_field_grant_access_access_policy',
    'reason' => t('Check the "Share Content" checkbox to mark content for public sharing.'),
  );
  $policies['user14'] = array(
    'access callback' => 'hook_policy_user14_access_policy',
    'reason' => t('Don't let Mark see this.'),
    'exclusive' => TRUE,
  );

  return $policies;
}

/**
 * Access callback for user14 policy.
 */
function hook_policy_user14_access_policy($node) {
  return $node->uid != 14;
}

/**
 * Access callback for field_grant_access policy.
 */
function hook_policy_field_grant_access_access_policy($node) {
  return (bool) $node->field_grant_access[0]['value'];
}

/**
 * Implementation of hook_og_privacy_policy_info_alter(). 
 *
 * @param $policies
 *  Associative array of policies.
 * @param $node
 *  The node object to be affected by the access policies.
 */
function hook_og_privacy_policy_info_alter(&$policies, $node) {
  // Allow "Bob" to gain access to the content some other way.
  $policies['user14']['exclusive'] = FALSE;
}
